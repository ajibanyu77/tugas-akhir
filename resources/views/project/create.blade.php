@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create Portofolio</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('project.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="title">Nama Portofolio</label>
          <input type="text" class="form-control" id="nama_project" name="nama_project" value="{{ old('nama_project','') }}" placeholder="Nama Project">
        </div>
        <div class="form-group">
            <label for="foto">Cover</label>
              <input type="file" class="uploads form-control" style="margin-top: 20px;" name="foto">
          </div>
        </div>

        <button type="submit" class="btn btn-primary" id="submit">
          Submit
        </button>
        <a href="/" class="btn btn-light pull-right">Back</a>
      </div>
  </div>
</form>
</div>
</div>
@endsection
