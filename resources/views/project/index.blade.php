@extends('layouts.app')

@section('content')
<div class="mt-3 ml-3 mr-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Project</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <a class="btn btn-danger mb-3" href="{{route('project.create')}}">Tambah Project</a>
          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama Project</th>
                <th>Image</th>
                <th>Nama Pembuat</th>
                <th style="width: 40px">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach($project as $key => $post)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td>{{$post->nama_project}}</td>
                    <td><img width="150px" src="{{ url('/data_file/'.$post->foto) }}"></td>
                    <td>{{$post->users->name}}</td>
                    <td style="display: flex;">
                        <a href="{{route('project.show', ['project' => $post->id])}}" class="btn btn-info btn-sm mr-1">
                            <i class="fa fa-eye"></i>
                        </a>
                        <form action="/project/{{$post->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i> 
                            </button>
                        </form>
                    </td>
                    
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
</div>
@endsection

