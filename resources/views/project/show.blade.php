@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-danger">
        <div class="card-header">
        <h3 class="card-title">Lihat Project Id Ke {{$project->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
    <form role="form" action="{{url('/project')}}" >
            @csrf <!--Token-->
        <div class="card-body">
            <div class="form-group">
            <label for="nama_project">Nama Project</label>
            <input type="nama_project" class="form-control" id="nama_project" name="nama_project" value="{{ old('nama_project', $project->nama_project) }}" readonly>
            @error('nama_project')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label for="foto">Foto</label>
                <div>
                    
                <img width="500" height="500" src="{{ url('/data_file/'.$project->foto) }}"/>
                </div>
                @error('foto')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Kembali</button>
        </div>
        </form>
    </div>    
</div>
    
@endsection
