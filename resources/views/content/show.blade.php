@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-danger">
        <div class="card-header">
        <h3 class="card-title">Lihat Project Id Ke {{$content->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
    <form role="form" action="{{url('/content')}}" >
            @csrf <!--Token-->
        <div class="card-body">
            <div class="form-group">
            <label for="judl">Nama Project</label>
            <input type="judul" class="form-control" id="judul" name="judul" value="{{ old('judul', $content->judul) }}" readonly>
            @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label>Isi Description</label>
                <textarea class="form-control" rows="15" id="isi" name="isi" value="" readonly>{{ old('isi', $content->isi)}}</textarea>
                @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label for="categories_id">Nama Kategori</label>
                <input type="categories_id" class="form-control" id="categories_id" name="categories_id" value="{{ old('judul', $content->kategori->nama_kategori) }}" readonly>
                @error('categories_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
            <div class="form-group">
                <label for="foto">Foto</label>
                <div>
                    
                <img width="500" height="500" src="{{ url('/data_file_content/'.$content->image) }}"/>
                </div>
                @error('foto')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Kembali</button>
        </div>
        </form>
    </div>    
</div>
    
@endsection
