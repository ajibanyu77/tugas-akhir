@extends('layouts.app')

@section('content')
<div class="row">

    <div class="col-lg-2">
        <a href="{{ url('content/create') }}" class="btn btn-primary btn-rounded btn-fw"><i class="fa fa-plus"></i> Tambah Content</a>
    </div>
    <div class="col-lg-12">
        @if (Session::has('message'))
        <div class="alert alert-{{ Session::get('message_type') }}" id="waktu2" style="margin-top:10px;">{{ Session::get('message') }}</div>
        @endif
    </div>
</div>
<div class="row" style="margin-top: 20px;">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">
                <h4 class="card-title">Data Content</h4>

                <div class="table-responsive">
                    <table class="table table-striped" id="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Category</th>
                                <th>User</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($content as $key => $post)
                            <tr>
                                <td> {{ $key + 1 }} </td>
                                <td><img width="150px" src="{{ url('/data_file_content/'.$post->image) }}"></td>
                                <td>{{$post->judul}}</td>
                                <td>{{$post->isi}}</td>
                                <td>{{$post->kategori}}</td>
                                <td>{{$post->users->name}}</td>
                                <td style="display: flex;">
                                    <a href="{{route('content.show', ['content' => $post->id])}}" class="btn btn-info btn-sm mr-1">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{route('content.edit', ['content' => $post->id])}}" class="btn btn-secondary btn-sm mr-1">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <form action="/content/{{$post->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash"></i> 
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
