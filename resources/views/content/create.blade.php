@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create Content</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('content.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="title">Judul</label>
          <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul','') }}" placeholder="Judul">
          @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label>Isi Description</label>
          <textarea class="form-control" rows="15" id="isi" name="isi" value="{{ old('isi','') }}" placeholder="Description ..."></textarea>
          @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label>Category</label>
          <select class="form-control" name="categories_id">
            <option value="">-pilih kategori-</option>
            @foreach ($categories as $item)
          <option value="{{$item->id}}" {{old('categories_id') == $item->id ? 'selected' : null}}>{{$item->nama_kategori}}</option>   
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="image" class="control-label">Cover</label>
            <input type="file" class="uploads form-control" style="margin-top: 20px;" name="image">
        </div>
      </div>
      <button type="submit" class="btn btn-primary" id="submit">Submit</button>
      <a href="/content" class="btn btn-light pull-right">Back</a>

</form>
</div>
</div>
@endsection

