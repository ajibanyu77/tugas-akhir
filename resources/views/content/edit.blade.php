@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-danger">
        <div class="card-header">
        <h3 class="card-title">Edit Content Id Ke {{$content->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
    <form role="form" action="/content/{{$content->id}}" method="POST">
        @csrf <!--Token-->
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
            <label for="judl">Nama Project</label>
            <input type="judul" class="form-control" id="judul" name="judul" value="{{ old('judul', $content->judul) }}">
            @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label>Isi Description</label>
                <textarea class="form-control" rows="15" id="isi" name="isi" value="">{{ old('isi', $content->isi)}}</textarea>
                @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="form-group">
                <label>Category</label>
                <select class="form-control" name="categories_id">
                  @foreach ($categories as $item)
                <option value="{{$item->id}}" {{old('categories_id') == $item->id ? 'selected' : null}}>{{$item->nama_kategori}}</option>   
                  @endforeach
                </select>
              </div>
            <div class="form-group">
                <label for="foto">Foto</label>
                <div>
                    
                <img width="500" height="500" src="{{ url('/data_file_content/'.$content->image) }}"/>
                <input type="file" class="uploads form-control" style="margin-top: 20px;" name="image">    
            </div>
                @error('image')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Edit</button>
            <button type="submit" class="btn btn-danger">Kembali</button>
        </div>
        </form>
    </div>    
</div>
    
@endsection
