<ul class="nav">
    <li class="nav-item nav-profile">
        <div class="nav-link">
            <div class="user-wrapper">
                <div class="profile-image">

                    <img src="{{asset('images/user/default.png')}}" alt="profile image">

                </div>
                <div class="text-wrapper">
                    <p class="profile-name"></p>
                    <div>
                        <small class="designation text-muted" style="text-transform: uppercase;letter-spacing: 1px;"></small>
                        <span class="status-indicator online"></span>
                    </div>
                </div>
            </div>
        </div>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/')}}">
            <i class="menu-icon mdi mdi-television"></i>
            <span class="menu-title">Dashboard</span>
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link " data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
            <i class="menu-icon mdi mdi-content-copy"></i>
            <span class="menu-title">Master Data</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('user')}}">Data User</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('content')}}">Data Content</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('categori')}}">Data Categories</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('project')}}">Data Profolio</a>
                </li>
            </ul>
        </div>
    </li>

</ul>