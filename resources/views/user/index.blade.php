@extends('layouts.app')

@section('content')
<div class="mt-3 ml-3 mr-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data User</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama User</th>
                <th>Email</th>
                <th style="width: 40px">Actions</th>
              </tr>
            </thead>
            <tbody>
              <!--looping untuk mengambil data-->
             @forelse ($users as $key => $post)
                  <tr>
                      <td> {{ $key + 1 }} </td>
                      <td> {{ $post->name }} </td>
                      <td> {{ $post->email }} </td>
                      <td style="display: flex;">
                          <a href="{{route('user.show', ['user' => $post->id])}}" class="btn btn-info btn-sm mr-1">
                              <i class="fa fa-eye"></i>
                          </a>
                          <form action="/user/{{$post->id}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-danger btn-sm">
                                  <i class="fa fa-trash"></i> 
                              </button>
                          </form>
                      </td>
                  </tr>
                  @empty
                  <tr>
                      <td colspan="4" align="center">Data Kosong</td>    
                  </tr>
              @endforelse                
          </tbody>
        </table>
          
        </div>
      </div>
</div>
@endsection
