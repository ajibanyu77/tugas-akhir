@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-danger">
        <div class="card-header">
        <h3 class="card-title">Lihat Users Id Ke {{$users->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
    <form role="form" action="{{url('/user')}}" >
            @csrf <!--Token-->
        <div class="card-body">
            <div class="form-group">
            <label for="name">Nama</label>
            <input type="name" class="form-control" id="name" name="name" value="{{ old('name', $users->name) }}" readonly>
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="card-body">
            <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $users->email) }}" readonly>
            @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Kembali</button>
        </div>
        </form>
    </div>    
</div>
    
@endsection
