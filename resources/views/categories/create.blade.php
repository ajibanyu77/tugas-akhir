@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create Categories</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('categori.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="title">Nama Kategori</label>
          <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" value="{{ old('nama_kategori','') }}" placeholder="Nama Categori">
          @error('nama_kategori')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <button type="submit" class="btn btn-primary" id="submit">
          Submit
        </button>
        <a href="/" class="btn btn-light pull-right">Back</a>

      </div>
  </div>
</div>

</form>
</div>
</div>
@endsection
