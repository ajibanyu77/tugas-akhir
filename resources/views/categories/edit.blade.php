@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-danger">
        <div class="card-header">
        <h3 class="card-title">Edit Pertanyaan Id Ke {{$kategori->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/categori/{{$kategori->id}}" method="POST">
            @csrf <!--Token-->
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                <label for="nama_kategori">Nama kategori</label>
                <input type="nama_kategori" class="form-control" id="nama_kategori" name="nama_kategori" value="{{ old('nama_kategori', $kategori->nama_kategori) }}">
                @error('nama_kategori')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>
            </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Edit</button>
            <a href="/categori" class="btn btn-secondary">Kembali</a>
        </div>
        </form>
    </div>    
</div>

@endsection
