<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        table {
            border-spacing: 0;
            width: 100%;
        }

        th {
            background: #404853;
            background: linear-gradient(#687587, #404853);
            border-left: 1px solid rgba(0, 0, 0, 0.2);
            border-right: 1px solid rgba(255, 255, 255, 0.1);
            color: #fff;
            padding: 8px;
            text-align: left;
            text-transform: uppercase;
        }

        th:first-child {
            border-top-left-radius: 4px;
            border-left: 0;
        }

        th:last-child {
            border-top-right-radius: 4px;
            border-right: 0;
        }

        td {
            border-right: 1px solid #c6c9cc;
            border-bottom: 1px solid #c6c9cc;
            padding: 8px;
        }

        td:first-child {
            border-left: 1px solid #c6c9cc;
        }

        tr:first-child td {
            border-top: 0;
        }

        tr:nth-child(even) td {
            background: #e8eae9;
        }

        tr:last-child td:first-child {
            border-bottom-left-radius: 4px;
        }

        tr:last-child td:last-child {
            border-bottom-right-radius: 4px;
        }

        img {
            width: 40px;
            height: 40px;
            border-radius: 100%;
        }

        .center {
            text-align: center;
        }
    </style>
    <link rel="stylesheet" href="">
    <title>Laporan Data Buku</title>
</head>

<body>
    <h1 class="center">LAPORAN DATA BUKU</h1>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama Project</th>
                <th>Image</th>
                <th style="width: 40px">Actions</th>
            </tr>
        </thead>
        <tbody>
            {{-- // @forelse ($pertanyaan as $key => $post)
                    <tr>
                        <td> {{ $key + 1 }} </td>
            <td> {{ $post->Nama Project }} </td>
            <td style="display: flex;">
                <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm mr-1">
                    <i class="fa fa-eye"></i>
                </a>
                <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-secondary btn-sm mr-1">
                    <i class="fa fa-edit"></i>
                </a>
                <form action="/pertanyaan/{{$post->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">Data Kosong</td>
            </tr>
            @endforelse --}}
        </tbody>
    </table>
</body>

</html>