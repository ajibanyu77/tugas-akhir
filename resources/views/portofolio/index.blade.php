@extends('layouts.app')

@section('content')
<div class="mt-3 ml-3 mr-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Project</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            <a class="btn btn-danger mb-3" href="{{route('portofolio.create')}}">Tambah Project</a>
            <a class="btn btn-success mb-3" href="{{route('portofolio.cetak_pdf')}}">Cetak PDF</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nama Project</th>
                        <th>Image</th>
                        <th style="width: 40px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- // @forelse ($pertanyaan as $key => $post)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                    <td> {{ $post->Nama Project }} </td>
                    <td style="display: flex;">
                        <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm mr-1">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-secondary btn-sm mr-1">
                            <i class="fa fa-edit"></i>
                        </a>
                        <form action="/pertanyaan/{{$post->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">Data Kosong</td>
                    </tr>
                    @endforelse --}}
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection