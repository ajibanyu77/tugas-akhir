<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = "contents";
    protected $guarded = [];
    
    public function kategori(){
        return $this->belongsTo('App\Categories', 'categories_id', 'id');
    }

    public function users(){
        return $this->belongsTo('App\Users', 'users_id', 'id');
    }
}
