<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "portofolios";
    protected $guarded = [];

    public function users(){
        return $this->belongsTo('App\Users', 'portofolios_id', 'id');
    }
}
