<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Content;
use App\Categories;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $content = Content::all();
        // return $content;
        return view('content.index', compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::all();
        // return $categories;
        return view('content.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'categories_id' => 'required',
        //     'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        // ]);

        $file = $request->file('image');
        $nama_file = time()."_".$file->getClientOriginalName();
          // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = "data_file_content";
        $file->move($tujuan_upload,$nama_file);
        
        Content::create([
            'judul'         => $request->judul,
            'isi'           => $request->isi,
            'image'         => $nama_file,
            'categories_id' => $request->categories_id,
            'users_id'      => Auth::id()
        ]);
            // return $content;
        return redirect('/content')->with('success', 'Content Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = Content::find($id);

        return view('content.show', compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $content = Content::find($id);
        $categories = Categories::all();
        return view('content.edit', compact('content', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // $file = $request->file('image');
        // if($file != ''  && $file != null){
        //     $file_old = $tujuan_upload.$file;
        //     unlink($file_old);
        // }
        
        // $nama_file = time()."_".$file->getClientOriginalName();
        // $tujuan_upload = "data_file_content";
        // $file->move($tujuan_upload,$nama_file);


        // $content = Content::find($id);

        // if($request->file('image') != ''){        
        //     $tujuan_upload = "data_file_content";
   
        //      //code for remove old file

   
        //      //upload new file
        //      $file = $request->file('image');
        //      $filename = time()."_".$file->getClientOriginalName();
        //      $file->move($tujuan_upload, $filename);
   
        //      //for update in table
        //     // $employee->update(['file' => $filename]);
        
        
        $update = Content::where('id', $id)->update([
            "judul"         => $request["judul"],
            "isi"           => $request["isi"],
            //"image"         => $nama_file,
            "categories_id" => $request["categories_id"],
            "users_id"      => Auth::id()
        ]);
        return redirect('/content')->with('success', 'Content Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Content::destroy($id);
        return redirect('/content')->with('success', 'Content Berhasil Di Hapus');
    }
}
